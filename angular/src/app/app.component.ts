import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dateSelectionForm = new FormGroup({
    from: new FormControl<string>('', {
      nonNullable: true,
      validators: [
        Validators.required
      ],
    }),
    to: new FormControl<string>(
      '',
      {
        nonNullable: true,
        validators: [Validators.required],
      }
    ),
  });

  loadPrices() {
    // load prices from backend and show them afterwards
    // load from http://localhost:8080/stats?from=2007-12-24T18:21Z&to=2007-12-24T18:21Z
    console.log(this.dateSelectionForm.controls.from.value);
    console.log(this.dateSelectionForm.controls.to.value);
  }
}
