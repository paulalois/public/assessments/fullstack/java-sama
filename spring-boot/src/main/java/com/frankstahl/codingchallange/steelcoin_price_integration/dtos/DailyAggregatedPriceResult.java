package com.frankstahl.codingchallange.steelcoin_price_integration.dtos;

import com.frankstahl.codingchallange.steelcoin_price_integration.domain.HistoricBuyPrice;

/**
 * contains the buy-price of that day and various other statistics
 */
public class DailyAggregatedPriceResult {
    private HistoricBuyPrice currentBuyPrice;
    private double averagePriceOfPastWeek;
    private HistoricBuyPrice minPriceOfPastWeek;
    private HistoricBuyPrice maxOfPastWeek;


}
