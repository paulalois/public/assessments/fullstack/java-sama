package com.frankstahl.codingchallange.steelcoin_price_integration.service.adapters;

import com.frankstahl.codingchallange.steelcoin_price_integration.adapters.SteelcoinHistoricPriceAdapter;
import com.frankstahl.codingchallange.steelcoin_price_integration.dtos.DailyAggregatedPriceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;

//HINT: Is component the correct Spring Bean Type for this type of class?
@Component
public class StatisticalAggregationService {
    private static final Logger LOG = LoggerFactory.getLogger(StatisticalAggregationService.class);
    private final SteelcoinHistoricPriceAdapter steelcoinHistoricPriceAdapter;

    public StatisticalAggregationService(SteelcoinHistoricPriceAdapter steelcoinHistoricPriceAdapter) {
        this.steelcoinHistoricPriceAdapter = steelcoinHistoricPriceAdapter;
    }

    public List<DailyAggregatedPriceResult> getAggregatedPrices(ZonedDateTime from, ZonedDateTime to) {
        LOG.info(from.toString());
        LOG.info(to.toString());
        var prices = steelcoinHistoricPriceAdapter.loadPricesFromTo(from, to);

        // HINT create your aggregation logic here.
        return List.of();
    }
}
