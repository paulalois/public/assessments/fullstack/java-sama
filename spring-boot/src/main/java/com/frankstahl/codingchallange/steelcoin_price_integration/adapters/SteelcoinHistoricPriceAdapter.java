package com.frankstahl.codingchallange.steelcoin_price_integration.adapters;

import com.frankstahl.codingchallange.steelcoin_price_integration.domain.HistoricBuyPrice;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;

@Component
public class SteelcoinHistoricPriceAdapter {
    public List<HistoricBuyPrice> loadPricesFromTo(ZonedDateTime from, ZonedDateTime to){
        // HINT: use the following CURL command and create some java code to query the
        // API and return a list of HistoricBuyPrice

        //curl 'https://pricing.steelcoin.com/data/steelcoin/legacy/volume-weighted-average' -X POST -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' -H 'Content-Type: application/json' -H 'Origin: https://steelcoin.com' --data-raw '{"from":"2023-02-21T12:00:30.332Z","to":"2023-03-23T12:00:30.332Z","resolution":"DAYS"}'
        return List.of();
    }
}
