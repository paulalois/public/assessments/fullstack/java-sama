package com.frankstahl.codingchallange.steelcoin_price_integration.controller;

import com.frankstahl.codingchallange.steelcoin_price_integration.dtos.DailyAggregatedPriceResult;
import com.frankstahl.codingchallange.steelcoin_price_integration.service.adapters.StatisticalAggregationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/")
public class PriceStatisticsController {
    private static final Logger LOG = LoggerFactory.getLogger(PriceStatisticsController.class);

    private final StatisticalAggregationService statisticalAggregationService;

    public PriceStatisticsController(@Autowired StatisticalAggregationService statisticalAggregationService){
        this.statisticalAggregationService = statisticalAggregationService;
    }

    @GetMapping("/stats")
    public List<DailyAggregatedPriceResult> getAggregatedPrices(ZonedDateTime from, ZonedDateTime to){
        // HINT: call your service and collect the required data from our API
        return statisticalAggregationService.getAggregatedPrices(from, to);
    }

    @GetMapping()
    public String getAggregatedPrices(
    ){
        return "hello world";
    }

}
