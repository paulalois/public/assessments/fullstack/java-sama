package com.frankstahl.codingchallange.steelcoin_price_integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SteelcoinPriceIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SteelcoinPriceIntegrationApplication.class, args);
	}

}
