package com.frankstahl.codingchallange.steelcoin_price_integration.domain;

import java.time.ZonedDateTime;

/**
 * describes the buy price of steelcoin on a certain day
 */
public class HistoricBuyPrice {
    private double buyPrice;
    private ZonedDateTime dateTime;
}
