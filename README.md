# Steelcoin Coding Challange
Hint: Parts of this story is fictional. All similarities with actually companies is a coincidence 😉  

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
- [ ] [Setup SSH](https://docs.gitlab.com/ee/user/ssh.html) to push and pull.
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push to this Git repository with your favorite IDE
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

## Description
This project is used for preparing applicants for coding interviews.
This project holds a spring boot project based on Java and an Angular 15 project based on Material Design.

## Story
Frankstahl is a traditional and successful steel trading company. 
Our customers can order steel products on [thesteel.com](https://thesteel.com)
With millions of turnover, this shop requires high availability, various K8s microservices and multiple deployment regions.

You, as our latest joiner, are challenged with an important task:
Our accounting department is currently being audited by the tax-agency and they need our urgent help.
Frankstahl recently founded a spin-off named Steelcoin. This spin-off buys and sells specially forged steel sheets from Frankstahl.

Customers of Steelcoin can invest into the relaibility of steel. Steelcoin, in the background, buys and sells steel plates and stores them in secret, highly secure warehouses.
You can learn more about Steelcoin at [steelcoin.com](https://steelcoin.com/)

## Task
#### In Java:
- Connect to the Steelcoin API and query the recent prices of Steelcoin.
- We prepared a `PriceStatisticsController` which
  - runs at `localhost:8080/stats?from=2007-12-24T18:21Z&to=2007-12-24T18:21Z` 
  - has start and end-time as input parameters and should
  - return a result object which contains the raw data and some statitics of the day. You find the object in the prepared controller.

#### In Angular
- We prepared a simple app-component.
- You need to connect the AngularForm to the backend.
- Use the data and display the result in an appealing way. No [json pipe](https://angular.io/api/common/JsonPipe) 😉

## Hints
We left some HINTS in this code base. You can find them by searching for HINT/Hint.

## License
This project should only be used by Frankstahl applicants. Don't share! 

## Too much?
If you run out of energy or time let us know.

We do not expect a perfect, run-able solution. 
We want to see how you approach and solve certain problems and have a baseline for our "code-review".

At Frankstahl we often develop feature branches and practise regular pair-programming sessions as well as code-reviews for MergeRequests.
Your coding assessment aims to give you a feeling for our task definitions.

It is similar to our day-to-day work. Based on customer requirements you start implementing and prototyping. Then we discuss the next steps.



